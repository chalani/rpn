<?php

function show_date_with_name_day()
{
	# MONTHS AND DAYS
	$months = array ('január', 'február', 'marec', 'apríl', 'máj', 'jún', 'júl', 'august', 'september', 'október', 'november', '12' => 'december');
	$days = array ('Nedeľa', 'Pondelok', 'Utorok', 'Streda', 'Štvrtok', 'Piatok', 'Sobota');

	# TODAY'S DATE
	$date = $days[date('w')] . ', ' . date('j') . '. ' . $months[date('n')] . ' ' . date('Y') . '.';

	# NAME DAY
	$name_day = array ('01' => array ('01' => '', '02' => 'Alexandra, Karina', '03' => 'Daniela', '04' => 'Drahoslav', '05' => 'Andrea', '06' => 'Antónia', '07' => 'Bohuslava', '08' => 'Severín', '09' => 'Alexej', '10' => 'Dáša', '11' => 'Malvína', '12' => 'Ernest', '13' => 'Rastislav', '14' => 'Radovan', '15' => 'Dobroslav', '16' => 'Kristína', '17' => 'Nataša', '18' => 'Bohdana', '19' => 'Drahomíra, Mário', '20' => 'Dalibor', '21' => 'Vincent', '22' => 'Zora', '23' => 'Miloš', '24' => 'Timotej', '25' => 'Gejza', '26' => 'Tamara', '27' => 'Bohuš', '28' => 'Alfonz', '29' => 'Gašpar', '30' => 'Ema', '31' => 'Emil'),
					   '02' => array ('01' => 'Tatiana', '02' => 'Erika, Erik', '03' => 'Blažej', '04' => 'Veronika', '05' => 'Agáta', '06' => 'Dorota', '07' => 'Vanda', '08' => 'Zoja', '09' => 'Zdenko', '10' => 'Gabriela', '11' => 'Dezider', '12' => 'Perla', '13' => 'Arpád', '14' => 'Valentín', '15' => 'Pravoslav', '16' => 'Ida, Liana', '17' => 'Miloslava', '18' => 'Jaromír', '19' => 'Vlasta', '20' => 'Lívia', '21' => 'Eleonóra', '22' => 'Etela', '23' => 'Roman, Romana', '24' => 'Matej', '25' => 'Frederik, Frederika', '26' => 'Viktor', '27' => 'Alexander', '28' => 'Zlatica', '29' => 'Radomír'),
					   '03' => array ('01' => 'Albín', '02' => 'Anežka', '03' => 'Bohumil, Bohumila', '04' => 'Kazimír', '05' => 'Fridrich', '06' => 'Radoslav, Radoslava', '07' => 'Tomáš', '08' => 'Alan, Alana', '09' => 'Františka', '10' => 'Bruno, Branislav', '11' => 'Angela, Angelika', '12' => 'Gregor', '13' => 'Vlastimil', '14' => 'Matilda', '15' => 'Svetlana', '16' => 'Boleslav', '17' => 'Ľubica', '18' => 'Eduard', '19' => 'Jozef', '20' => 'Víťazoslav, Klaudius', '21' => 'Blahoslav', '22' => 'Beňadik', '23' => 'Adrián', '24' => 'Gabriel', '25' => 'Marián', '26' => 'Emanuel', '27' => 'Alena', '28' => 'Soňa', '29' => 'Miroslav', '30' => 'Vieroslava', '31' => 'Benjamín'),
					   '04' => array ('01' => 'Hugo', '02' => 'Zita', '03' => 'Richard', '04' => 'Izidor', '05' => 'Miroslava', '06' => 'Irena', '07' => 'Zoltán', '08' => 'Albert', '09' => 'Milena', '10' => 'Igor', '11' => 'Július', '12' => 'Estera', '13' => 'Aleš', '14' => 'Justína', '15' => 'Fedor', '16' => 'Dana, Danica', '17' => 'Rudolf', '18' => 'Valér', '19' => 'Jela', '20' => 'Marcel', '21' => 'Ervín', '22' => 'Slavomír', '23' => 'Vojtech', '24' => 'Juraj', '25' => 'Marek', '26' => 'Jaroslava', '27' => 'Jaroslav', '28' => 'Jarmila', '29' => 'Lea', '30' => 'Anastázia'),
					   '05' => array ('01' => '', '02' => 'Žigmund', '03' => 'Galina', '04' => 'Florián', '05' => 'Lesia, Lesana', '06' => 'Hermína', '07' => 'Monika', '08' => 'Ingrida', '09' => 'Roland', '10' => 'Viktória', '11' => 'Blažena', '12' => 'Pankrác', '13' => 'Servác', '14' => 'Bonifác', '15' => 'Žofia, Sofia', '16' => 'Svetozár', '17' => 'Gizela', '18' => 'Viola', '19' => 'Gertrúda', '20' => 'Bernard', '21' => 'Zina', '22' => 'Júlia, Juliana', '23' => 'Želmíra', '24' => 'Ela', '25' => 'Urban', '26' => 'Dušan', '27' => 'Iveta', '28' => 'Viliam', '29' => 'Vilma', '30' => 'Ferdinand', '31' => 'Petrana, Petronela'),
					   '06' => array ('01' => 'Žaneta', '02' => 'Xénia, Oxana', '03' => 'Karolína', '04' => 'Lenka', '05' => 'Laura', '06' => 'Norbert', '07' => 'Róbert', '08' => 'Medard', '09' => 'Stanislava', '10' => 'Margaréta', '11' => 'Dobroslava', '12' => 'Zlatko', '13' => 'Anton', '14' => 'Vasil', '15' => 'Vít', '16' => 'Blanka, Bianka', '17' => 'Adolf', '18' => 'Vratislav', '19' => 'Alfréd', '20' => 'Valéria', '21' => 'Alojz', '22' => 'Paulína', '23' => 'Sidónia', '24' => 'Ján', '25' => 'Olívia, Tadeáš', '26' => 'Adriána', '27' => 'Ladislav, Ladislava', '28' => 'Beáta', '29' => 'Peter, Pavol, Petra', '30' => 'Melánia'),
					   '07' => array ('01' => 'Diana', '02' => 'Berta', '03' => 'Miloslav', '04' => 'Prokop', '05' => 'cyril, Metod', '06' => 'Patrik, Patrícia', '07' => 'Oliver', '08' => 'Ivan', '09' => 'Lujza', '10' => 'Amália', '11' => 'Milota', '12' => 'Nina', '13' => 'Margita', '14' => 'Kamil', '15' => 'Henrich', '16' => 'Drahomír', '17' => 'Bohuslav', '18' => 'Kamila', '19' => 'Dušana', '20' => 'Iľja, Eliáš', '21' => 'Daniel', '22' => 'Magdaléna', '23' => 'Oľga', '24' => 'Vladimír', '25' => 'Jakub', '26' => 'Anna, Hana, Anita', '27' => 'Božena', '28' => 'Krištof', '29' => 'Marta', '30' => 'Libuša', '31' => 'Ignác'),
					   '08' => array ('01' => 'Božidara', '02' => 'Gustáv', '03' => 'Jerguš', '04' => 'Dominika, Dominik', '05' => 'Hortenzia', '06' => 'Jozefína', '07' => 'Štefánia', '08' => 'Oskar', '09' => 'Ľubomíra', '10' => 'Vavrinec', '11' => 'Zuzana', '12' => 'Darina', '13' => 'Ľubomír', '14' => 'Mojmír', '15' => 'Marcela', '16' => 'Leonard', '17' => 'Milica', '18' => 'Elena, Helena', '19' => 'Lýdia', '20' => 'Anabela, Liliana', '21' => 'Jana', '22' => 'Tichomír', '23' => 'Filip', '24' => 'Bartolomej', '25' => 'Ľudovít', '26' => 'Samuel', '27' => 'Silvia', '28' => 'Augustín', '29' => 'Nikola, Nikolaj', '30' => 'Ružena', '31' => 'Nora'),
					   '09' => array ('01' => 'Drahoslava', '02' => 'Linda, Rebeka', '03' => 'Belo', '04' => 'Rozália', '05' => 'Regina', '06' => 'Alica', '07' => 'Marianna', '08' => 'Miriama', '09' => 'Martina', '10' => 'Oleg', '11' => 'Bystrík', '12' => 'Mária, Marlena', '13' => 'Ctibor', '14' => 'Ľudomil', '15' => 'Jolana', '16' => 'Ľudmila', '17' => 'Olympia', '18' => 'Eugénia', '19' => 'Konštantín', '20' => 'Ľuboslav, Ľuboslava', '21' => 'Matúš', '22' => 'Móric', '23' => 'Zdenka', '24' => 'Ľuboš, Ľubor', '25' => 'Vladislav, Vladislava', '26' => 'Edita', '27' => 'Cyprián', '28' => 'Václav', '29' => 'Michal, Michaela', '30' => 'Jarolím'),
					   '10' => array ('01' => 'Arnold', '02' => 'Levoslav', '03' => 'Stela', '04' => 'František', '05' => 'Viera', '06' => 'Natália', '07' => 'Eliška', '08' => 'Brigita', '09' => 'Dionýz', '10' => 'Slavomíra', '11' => 'Valentína', '12' => 'Maximilián', '13' => 'Koloman', '14' => 'Boris', '15' => 'Leopold', '16' => 'Agnesa', '17' => 'Klaudia', '18' => 'Eugen', '19' => 'Alžbeta', '20' => 'Félix', '21' => 'Elvíra', '22' => 'Cecília', '23' => 'Klement', '24' => 'Emília', '25' => 'Katarína', '26' => 'Kornel', '27' => 'Milan', '28' => 'Henrieta', '29' => 'Vratko', '30' => 'Ondrej, Andrej'),
					   '11' => array ('01' => 'Denisa, Denis', '02' => '', '03' => 'Hubert', '04' => 'Karol', '05' => 'Imrich', '06' => 'Renáta', '07' => 'René', '08' => 'Bohumír', '09' => 'Teodor', '10' => 'Tibor', '11' => 'Martin, Maroš', '12' => 'Svätopluk', '13' => 'Stanislav', '14' => 'Irma', '15' => 'Leopold', '16' => 'Agnesa', '17' => 'Klaudia', '18' => 'Eugen', '19' => 'Alžbeta', '20' => 'Félix', '21' => 'Elvíra', '22' => 'Cecília', '23' => 'Klement', '24' => 'Emília', '25' => 'Katarína', '26' => 'Kornel', '27' => 'Milan', '28' => 'Henrieta', '29' => 'Vratko', '30' => 'Ondrej, Andrej'),
					   '12' => array ('01' => 'Edmund', '02' => 'Bibiána', '03' => 'Oldrich', '04' => 'Barbora, Barbara', '05' => 'Oto', '06' => 'Mikuláš', '07' => 'Ambróz', '08' => 'Marína', '09' => 'Izabela', '10' => 'Radúz', '11' => 'Hilda', '12' => 'Otília', '13' => 'Lucia', '14' => 'Branislava, Bronislava', '15' => 'Ivica', '16' => 'Albína', '17' => 'Kornélia', '18' => 'Sláva', '19' => 'Judita', '20' => 'Dagmara', '21' => 'Bohdan', '22' => 'Adela', '23' => 'Nadežda', '24' => 'Adam, Eva', '25' => '', '26' => 'Štefan', '27' => 'Filoména', '28' => 'Ivana, Ivona', '29' => 'Milada', '30' => 'Dávid', '31' => 'Silvester'));

	$name_day = ' <span>Meniny má ' . $name_day[date('m')][date('d')] . '</span>, zajtra ' . $name_day[date('m')][date('d', time() + 86400)] . '.';

	# OUTPUT
	echo '<div id="show-date-with-name-day">' . $date . $name_day . '</div>';
}

?>
<!DOCTYPE html>
<html lang="sk">
<head>
<title>Regionálne noviny - Vaše online spravodajstvo</title>
<meta charset="utf-8">
<meta name="author" content="regionWEB">
<meta name="robots" content="noindex, nofollow">
<link href="style.css" rel="stylesheet" media="all">
<script src="plugins.js"></script>
<script src="scripts.js"></script>
</head>
<body>
	<div id="wrapper">
		<header>
			<div id="title">regionálne noviny <span>online</span></div>
			<a href="" title="" id="login">Pridať článok</a>
			<?php show_date_with_name_day(); ?>
			<nav>
				<ul>
					<li><a href="/alpha" title=""><span>Domov</span></a></li>
					<li><a href="obcianske-spravy.php" title="">Občianske správy</a></li>
					<li><a href="regionalne-spravy.php" title="">Správy z regiónov</a></li>
					<li><a href="obcianska-inzercia.php" title="">Občianska inzercia</a></li>
					<li><a href="ponuky-prace.php" title="">Ponuky práce</a></li>
					<li class="active"><a href="zlavy.php" title="">Zľavy</a></li>
				</ul>
			</nav>
		</header>

		<article class="discounts">
			<div class="heading"><h1>Zľavy</h1></div>

			<section class="post">
				<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
				<h2>Lorem ipsum</h2>
				<div class="meta"><span>1. január 1970</span><span>Celé Slovensko</span></div>
				<ul>
					<li>Pôvodná cena: 1 234 €</li>
					<li><strong>Naša cena: <span>567 €</span></strong></li>
					<li>Zľava: 56 %</li>
				</ul>
			</section>
			<section class="post">
				<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
				<h2>Lorem ipsum</h2>
				<div class="meta"><span>1. január 1970</span><span>Celé Slovensko</span></div>
				<ul>
					<li>Pôvodná cena: 1 234 €</li>
					<li><strong>Naša cena: <span>567 €</span></strong></li>
					<li>Zľava: 56 %</li>
				</ul>
			</section>
			<section class="post">
				<img src="http://lorempixel.com/308/188/nature/3" width="308" height="188" alt="">
				<h2>Lorem ipsum</h2>
				<div class="meta"><span>1. január 1970</span><span>Celé Slovensko</span></div>
				<ul>
					<li>Pôvodná cena: 1 234 €</li>
					<li><strong>Naša cena: <span>567 €</span></strong></li>
					<li>Zľava: 56 %</li>
				</ul>
			</section>
			<section class="post">
				<img src="http://lorempixel.com/308/188/nature/4" width="308" height="188" alt="">
				<h2>Lorem ipsum</h2>
				<div class="meta"><span>1. január 1970</span><span>Celé Slovensko</span></div>
				<ul>
					<li>Pôvodná cena: 1 234 €</li>
					<li><strong>Naša cena: <span>567 €</span></strong></li>
					<li>Zľava: 56 %</li>
				</ul>
			</section>
			<section class="post">
				<img src="http://lorempixel.com/308/188/nature/5" width="308" height="188" alt="">
				<h2>Lorem ipsum</h2>
				<div class="meta"><span>1. január 1970</span><span>Celé Slovensko</span></div>
				<ul>
					<li>Pôvodná cena: 1 234 €</li>
					<li><strong>Naša cena: <span>567 €</span></strong></li>
					<li>Zľava: 56 %</li>
				</ul>
			</section>
		</article>

		<aside class="tips">
			<div class="heading"><h1>Užitočné informácie</h1></div>

			<ul>
				<li><a href="" title="">Lorem ipsum</a></li>
				<li><a href="" title="">Dolor sit amet</a></li>
				<li><a href="" title="">Consectetur adipiscing</a></li>
				<li><a href="" title="">Integer nec odio</a></li>
				<li><a href="" title="">Praesent libero</a></li>
				<li><a href="" title="">Sed cursus ante dapibus</a></li>
			</ul>
		</aside>

		<aside>
			<a href="" title="" id="reader"><span>Čítajte nás ON-LINE</span></a>
		</aside>

		<aside class="advertising">
			<div class="heading"><h1>Reklama</h1></div>

			<div class="title">Lorem ipsum dolor sit amet</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
			<div class="title">Consectetur adipiscing elit</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel.</div>
			<div class="title">Duis sagittis ipsum</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
		</aside>

		<footer>
			regionPRESS, s.r.o. &#169; <?php echo date( 'Y' ); ?>
		</footer>
	</div>
</body>