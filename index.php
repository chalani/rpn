<?php require 'name-days.php'; ?>
<!DOCTYPE html>
<html lang="sk">
<head>
<title>Regionálne noviny - Vaše online spravodajstvo</title>
<meta charset="utf-8">
<meta name="author" content="regionWEB">
<meta name="robots" content="noindex, nofollow">
<link href="style.css" rel="stylesheet" media="all">
<script src="plugins.js"></script>
<script src="scripts.js"></script>
</head>
<body>
	<div id="wrapper">
		<header>
			<div id="title">regionálne noviny <span>online</span></div>
			<a href="" title="" id="login">Pridať článok</a>
			<?php echo show_date_with_name_day(); ?>
			<nav>
				<ul>
					<li><a href="/alpha" title=""><span>Domov</span></a></li>
					<li><a href="obcianske-spravy.php" title="">Občianske správy</a></li>
					<li><a href="regionalne-spravy.php" title="">Správy z regiónov</a></li>
					<li><a href="obcianska-inzercia.php" title="">Občianska inzercia</a></li>
					<li><a href="ponuky-prace.php" title="">Ponuky práce</a></li>
					<li><a href="zlavy.php" title="">Zľavy</a></li>
				</ul>
			</nav>
		</header>

		<article>
			<section class="latest-posts">
				<div class="slides">
					<a href="" title="">
						<img src="http://lorempixel.com/640/400/nature/1" width="640" height="400" alt="">
						<div class="perex">
							<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
						</div>
					</a>
					<a href="" title="">
						<img src="http://lorempixel.com/640/400/nature/2" width="640" height="400" alt="">
						<div class="perex">
							<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
						</div>
					</a>
					<a href="" title="">
						<img src="http://lorempixel.com/640/400/nature/3" width="640" height="400" alt="">
						<div class="perex">
							<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
						</div>
					</a>
					<a href="" title="">
						<img src="http://lorempixel.com/640/400/nature/4" width="640" height="400" alt="">
						<div class="perex">
							<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
						</div>
					</a>
					<a href="" title="">
						<img src="http://lorempixel.com/640/400/nature/5" width="640" height="400" alt="">
						<div class="perex">
							<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
						</div>
					</a>
					<a href="#" title="Predošlá správa" class="slidesjs-previous slidesjs-navigation"><span>Predošlá správa</span></a>
					<a href="#" title="Nasledujúca správa" class="slidesjs-next slidesjs-navigation"><span>Nasledujúca správa</span></a>
				</div>
			</section>

			<section class="top-post">
				<a href="" title="">
					<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
					<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero.</p>
				</a>
			</section>

			<section class="other-posts">
				<div class="heading"><h1>Ďalšie správy</h1></div>

				<a href="" class="static">
					<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
					<h2>Lorem ipsum</h2>
					<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam...</p>
				</a>

				<div class="slides">
					<div>
						<a href="" title="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
					</div>
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/5" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/6" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/7" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/8" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
					</div>
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/9" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/10" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/11" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/82/82/nature/12" width="82" height="82" alt="">
							<h2>Lorem ipsum</h2>
							<div class="meta"><span>1. január 1970</span><span>Informačné technológie</span></div>
						</a>
					</div>
				</div>
			</section>

			<section class="discounts">
				<div class="heading"><h1>Zľavy</h1></div>

				<div class="slides">
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
					</div>
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
					</div>
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
					</div>
					<div>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/1" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
						<a href="" title="">
							<img src="http://lorempixel.com/308/188/nature/2" width="308" height="188" alt="">
							<div class="meta"><span>-56%</span><span>124 €</span></div>
							<h2>Lorem ipsum</h2>
						</a>
					</div>
<?php

/*
$megazlava = 'http://megazlava.sk/xml/zlavy.xml';
$xml = simplexml_load_file( $megazlava );

foreach ( $xml as $node ){
	echo $node->title;
}
*/

?>
				</div>
			</section>
		</article>

		<aside class="tips">
			<div class="heading"><h1>Užitočné informácie</h1></div>

			<ul>
				<li><a href="" title="">Lorem ipsum</a></li>
				<li><a href="" title="">Dolor sit amet</a></li>
				<li><a href="" title="">Consectetur adipiscing</a></li>
				<li><a href="" title="">Integer nec odio</a></li>
				<li><a href="" title="">Praesent libero</a></li>
				<li><a href="" title="">Sed cursus ante dapibus</a></li>
			</ul>
		</aside>

		<aside>
			<a href="" title="" id="reader"><span>Čítajte nás ON-LINE</span></a>
		</aside>

		<aside class="advertising">
			<div class="heading"><h1>Reklama</h1></div>

			<div class="title">Lorem ipsum dolor sit amet</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
			<div class="title">Consectetur adipiscing elit</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel.</div>
			<div class="title">Duis sagittis ipsum</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
		</aside>

		<footer>
			regionPRESS, s.r.o. &#169; <?php echo date( 'Y' ); ?>
		</footer>
	</div>
</body>