/* HTML5 */
if(navigator.userAgent.match(/msie\s(?!9.0)/i)){(function(){var elements=['address','article','aside','audio','canvas','command','datalist','details','dialog','figure','figcaption','footer','header','hgroup','keygen','mark','meter','menu','nav','progress','ruby','section','time','video'];for(var i=0;i<elements.length;i++){document.createElement(elements[i]);}})();}

/* Google Analytics */
var _gaq=[['_setAccount',''],['_trackPageview']];(function(d,t){var ga=d.createElement(t),s=d.getElementsByTagName(t)[0];ga.src=(document.location.protocol=='https:'?'https://':'http://')+'google-analytics.com/ga.js';ga.async=true;s.parentNode.insertBefore(ga,s)}(document,'script'))

/* Slides 3.0.3 [http://slidesjs.com] */
$(function(){$('.latest-posts .slides').slidesjs({width:640,height:400,pagination:false,navigation:false,play:{auto:true,interval:9000}});});
$(function(){$('.other-posts .slides').slidesjs({width:308,height:400,navigation:false});});
$(function(){$('.discounts .slides').slidesjs({width:308,height:400,navigation:false});});
