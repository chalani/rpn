<!DOCTYPE html>
<html lang="sk">
<head>
<title>Regionálne noviny - Vaše online spravodajstvo</title>
<meta charset="utf-8">
<meta name="author" content="regionWEB">
<meta name="robots" content="noindex, nofollow">
<link href="style.css" rel="stylesheet" media="all">
<script src="plugins.js"></script>
<script src="scripts.js"></script>
</head>
<body>
	<div id="wrapper">
		<header>
			<div id="title">regionálne noviny <span>online</span></div>
			<a href="" title="" id="login"><span>+</span>Pridať článok</a>
			<nav>
				<ul>
					<li><a href="/alpha" title=""><span>Domov</span></a></li>
					<li><a href="obcianske-spravy.php" title="">Občianske správy</a></li>
					<li><a href="obcianska-inzercia.php" title="">Občianska inzercia</a></li>
					<li><a href="regionalne-spravy.php" title="">Správy z regiónov</a></li>
					<li><a href="ponuky-prace.php" title="">Ponuky práce</a></li>
					<li><a href="zlavy.php" title="">Zľavy</a></li>
				</ul>
			</nav>
		</header>

		<article>

		</article>

		<aside class="tips">
			<div class="heading"><h1>Kategórie</h1></div>

			<ul>
				<li><a href="" title="">Lorem ipsum</a></li>
				<li><a href="" title="">Dolor sit amet</a></li>
				<li><a href="" title="">Consectetur adipiscing</a></li>
				<li><a href="" title="">Integer nec odio</a></li>
				<li><a href="" title="">Praesent libero</a></li>
				<li><a href="" title="">Sed cursus ante dapibus</a></li>
			</ul>
		</aside>

		<aside class="advertising">
			<div class="heading"><h1>Reklama</h1></div>

			<div class="title">Lorem ipsum dolor sit amet</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
			<div class="title">Consectetur adipiscing elit</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel.</div>
			<div class="title">Duis sagittis ipsum</div>
			<div class="desc">Proin ut ligula vel nunc egestas porttitor.</div>
		</aside>

		<aside class="reader">
			<img src="images/reader.jpg" width="280" height="96" alt="">
		</aside>

		<aside class="advertising">
			<div class="heading"><h1>Reklama</h1></div>

			<img src="images/joke.png" width="280" height="111" alt="">
		</aside>

		<footer>
			regionPRESS, s.r.o. &#169; <?php echo date( 'Y' ); ?>
		</footer>
	</div>
</body>